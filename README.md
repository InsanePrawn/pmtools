# pmOS-devtools

This repo contains a few useful tools for developing mainline with postmarketOS, they should work nicely with any msm8998 or newer device. `mkbootimg.sh` depends on pmbootstrap being installed and configured for your device.

* The [pmos-installer](https://gitlab.com/sdm845-mainline/pmos-installer) is now in it's own repo
* The [creategadget.sh](./creategadget.sh) script is not thoroughly tested and might need some tweaks.
* The [pmenv](./pmenv) file can be sourced to provide a simpler environment for cross compiling.
* [mkbootimg.sh](./mkbootimg.sh) is a wrapper for `mkbootimg` with defaults to make the process of testing boot images much simpler, source `pmenv` and then run `mkb -h` for help
---

# Installing

Clone this repo
```sh
mkdir -p ~/pmos && cd ~/pmos
git clone --recursive https://gitlab.com/sdm845-mainline/pmtools.git tools
```

Source `pmenv` in your bashrc or zshrc
```sh
echo "source \"~/pmos/tools/pmenv\"" >> ~/.bashrc
```

# Kernel build and flash

* `mm` is the pmenv make alias, see `alias mm` for details.
* `mkb` is an alias to run `$TOOLSDIR/mkbootimg.sh`, see `mkb -h` for details.

The kernel output directory is `.output`
## Instructions

```sh
# Generate kconfig
mm defconfig
# Build kernel
mm
# Build initrd and Android boot image
mkb -d <path to dtb> -c "rootfs_part=sda14 rootfs_path=.stowaways/postmarketOS.img" -o ~/pmos-boot.img
```

* If no `-d` arg is passed, `mkbootimg.sh` will read the pmos deviceinfo file for the currently selected device and select the dtb from there.
* If `rootfs_path` is not set, the initramfs will treat `rootfs_part` as root (so you can flash the rootfs directly to a partition).

You can now flash the boot image with `fastboot flash ~/pmos-boot.img`.

## Installing Rootfs

You can easily deploy a postmarketOS rootfs to your device as follows:

1. Build a normal postmarketOS image, but use `--split`, then export the image.
```sh
pmbootstrap install --split
pmbootstrap export
```
2. Copy image to device, via adb for example (in TWRP)
```
adb shell mkdir /data/.stowaways
adb push $(readlink /tmp/postmarketOS-export/VENDOR-CODENAME-root.img) /data/.stowaways/postmarketOS.img
```

## Booting

By default the initramfs will try and boot a postmarketOS rootfs image from `.stowaways/postmarketOS.img` on the `/dev/sda17` partition. These options are configured via the kernel cmdline and can be changed as followed:

```sh
# Read the cmdline
abootimg -i ~/pmos-boot.img
# Update the cmdline to match your device partition and rootfs path
abootimg -u ~/pmos-boot.img -c "cmdline=rootfs_part=sda17 rootfs_path=.stowaways/postmarketOS.img"
```


# usbnet

[usbnet.sh](./usbnet.sh) is a simple script to give your gadget device networking through your host, it can be setup as follows:

```sh
scp usbnet.sh $USER@172.16.42.1:/home/$USER/
ssh $USER@172.16.42.1 '$HOME/usbnet.sh'
```

## Host setup

### Arch/Ubuntu/Alpine

```sh
sudo sysctl net.ipv4.ip_forward=1                               
sudo iptables -P FORWARD ACCEPT
sudo iptables -A POSTROUTING -t nat -j MASQUERADE -s 172.16.42.0/24
```

### Fedora

First run
```sh
nmcli connection
```
To get the list of connections, fill in YOUR HOST internet connection for the first modify, then the connection to the device as the second.

```sh
sudo sysctl net.ipv4.ip_forward=1
sudo firewall-cmd --get-active-zone 
sudo nmcli connection modify '<NAME OF HOST INTERNET>' connection.zone external
sudo nmcli connection modify '<NAME OF CLIENT CONNECTION>' connection.zone internal
sudo firewall-cmd --get-active-zone 
sudo firewall-cmd --zone=external --add-masquerade
sudo firewall-cmd --zone=external --list-all
sudo firewall-cmd --zone=internal --list-all
```

Your device should now have internet access.

---
# Kernel things

> **NOTE:** I will be using aliases defined in [pmenv](./pmenv), you will need to source the file to use the same aliases.

## ccache

Run the follow commands to setup ccache for 'aarch64-linux-gnu-*', tested on Arch Linux, paths may need to be changed for other systems.

```
cd /usr/lib/ccache/bin
find /usr/bin -name "aarch64-linux-gnu-*" -printf "%f\n" | sudo xargs -n1 -I {} ln -s /usr/bin/ccache {}
```

Assuming ccache comes before `/usr/bin` in your path this will cause it to be hit, run `watch -n1 ccache -s` and see if you get cache hits while compiling. Also check with `alias m` after re-sourcing pmenv to see if the correct path is being hit, it should look like below:

```
❯ alias m
m='CC=/usr/lib/ccache/bin/aarch64-linux-gnu-gcc CROSS_COMPILE=/usr/lib/ccache/bin/aarch64-linux-gnu- make O=.output/ ARCH=arm64 -j9'
```

## Installing modules

> **NOTE:** This requires that you install the `rsync` package on your device.

```sh
m INSTALL_MOD_PATH=modules modules_install
rsync -a -e ssh .output/modules/lib/modules 172.16.42.1:/home/$USER/
ssh -t 172.16.42.1 sudo cp -r /home/$USER/modules /lib/
```

Alternatively use the `makeinstallmodules` alias in [`pmenv`](./pmenv).