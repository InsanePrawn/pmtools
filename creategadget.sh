#!/bin/sh

# See: https://www.kernel.org/doc/Documentation/usb/gadget_configfs.txt

mkdir -p /config
mount none -t configfs /config
CONFIGFS=/config/usb_gadget

if ! [ -e "$CONFIGFS" ]; then
    echo "  /config/usb_gadget does not exist, skipping configfs usb gadget"
    return
fi

echo "  Setting up an USB gadget through configfs"
echo "Create an usb gadet configuration"
mkdir $CONFIGFS/g1 || echo "  Couldn't create $CONFIGFS/g1"
printf "%s" "0x18D1" >"$CONFIGFS/g1/idVendor"
printf "%s" "0xD001" >"$CONFIGFS/g1/idProduct"

echo "Create english (0x409) strings"
mkdir $CONFIGFS/g1/strings/0x409 || echo "  Couldn't create $CONFIGFS/g1/strings/0x409"
echo "postmarketOS" > "$CONFIGFS/g1/strings/0x409/manufacturer"
echo "Debug network interface" > "$CONFIGFS/g1/strings/0x409/product"
echo "7d97ad74" > "$CONFIGFS/g1/strings/0x409/serialnumber"

echo "Create ncm function"
mkdir $CONFIGFS/g1/functions/ncm.usb0 || echo "  Couldn't create $CONFIGFS/g1/functions/ncm.usb0"

echo "Create configuration instance for the gadget"
mkdir $CONFIGFS/g1/configs/c.1 || echo "  Couldn't create $CONFIGFS/g1/configs/c.1"
mkdir $CONFIGFS/g1/configs/c.1/strings/0x409 || echo "  Couldn't create $CONFIGFS/g1/configs/c.1/strings/0x409"
printf "%s" "ncm" > $CONFIGFS/g1/configs/c.1/strings/0x409/configuration || echo "  Couldn't write configration name"

echo "Link the ncm instance to the configuration"
ln -s $CONFIGFS/g1/functions/ncm.usb0 $CONFIGFS/g1/configs/c.1 || echo "  Couldn't symlink ncm.usb0"

echo "Calling the usb controller"
echo "$(ls /class/udc)" > $CONFIGFS/g1/UDC || echo "  Couldn't write UDC"