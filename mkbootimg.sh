#!/bin/bash

# Source the deviceinfo file for the current pmos device
DEVICEINFO_FILE="$(pmbootstrap config aports)/device/testing/device-$(pmbootstrap config device)/deviceinfo"
[[ ! -f $DEVICEINFO_FILE ]] && echo "Can't find deviceinfo file" && exit 1

source $DEVICEINFO_FILE

TOOLSDIR="$(dirname $0)"
OUT="$HOME/pmos/mainline-boot.img"
DTB=".output/arch/arm64/boot/dts/${deviceinfo_dtb}.dtb"
IMAGE=".output/arch/arm64/boot/Image.gz"
RAMDISK="/tmp/initrd-mkb.cpio.gz"
# SDM845 defaults
#CMDLINE="rootfs_part=sda14 rootfs_path=.stowaways/postmarketOS.img"

THIS="$(basename $0)"

die() {
    echo $1 1>&2
    exit
}

usage() {
    echo "$THIS [ -d DTB(=$DTB) ] [ -r RAMDISK] [ -o OUT(=$OUT) ] [ -k KERNEL_IMAGE ] [ -c CMDLINE ]"
	echo "By default this will create a boot image that boots pmos from sda17, modify the cmdline parameters with 'abootimg -u' to change."
    exit 1
}

while getopts "r:o:d:k:c:e:h" options; do
	case "${options}" in
		r)
			RAMDISK=${OPTARG}
			CUSTOM_RD=true
			;;
		o)
			OUT=${OPTARG}
			;;
		d)
			DTB=${OPTARG}
			;;
		k)
			IMAGE=${OPTARG}
			;;
		c)
			CMDLINE_EXTRA=${OPTARG}
			;;
		h)
			usage
			;;
		:)
			die "-${OPTARG} requires an argument"
			;;
		*)
			usage
			;;
	esac
done

[[ -f "$IMAGE" ]] || die "Kernel image \"$IMAGE\" doesn't exist"
if ! [[ "$DTB" = "" ]] || [[ "$DTB" = "/dev/null" ]]; then
	[[ -f "$DTB" ]] || die "Device Tree Blob \"$DTB\" doesn't exist"

	echo -e "$THIS: Appending dtb \"./$DTB\" to image \"./$IMAGE\""
	cat "$IMAGE" "$DTB" > /tmp/kernel-dtb || die "Failed to append dtb"
else
	cp "$IMAGE" /tmp/kernel-dtb
fi

CMDLINE="$CMDLINE $CMDLINE_EXTRA"

CMD="mkbootimg \
	--base $deviceinfo_flash_offset_base \
	--kernel_offset $deviceinfo_flash_offset_kernel \
	--ramdisk_offset $deviceinfo_flash_offset_ramdisk \
	--tags_offset $deviceinfo_flash_offset_tags \
	--pagesize $deviceinfo_flash_pagesize \
	--second_offset $deviceinfo_flash_offset_second \
	--cmdline \"$CMDLINE\" \
	--kernel /tmp/kernel-dtb -o $OUT"

if [ -z $CUSTOM_RD ]; then
	$TOOLSDIR/initrd/build.sh $RAMDISK
fi

if [ ! -f $RAMDISK ]; then
	echo "$RAMDISK not found"
	exit 1
fi

CMD="$CMD --ramdisk $RAMDISK"

echo $CMD
eval $CMD || echo "Failed to make $OUT"
echo "$THIS: Boot image at $OUT"
