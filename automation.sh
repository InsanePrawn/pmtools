# Utils for automating the development cycle on a pmos device

([[ -n $ZSH_EVAL_CONTEXT && $ZSH_EVAL_CONTEXT =~ :file$ ]] || 
 [[ -n $KSH_VERSION && $(cd "$(dirname -- "$0")" &&
    printf '%s' "${PWD%/}/")$(basename -- "$0") != "${.sh.file}" ]] || 
 [[ -n $BASH_VERSION ]] && (return 0 2>/dev/null)) && sourced=1 || sourced=0

if [ $sourced -eq 0 ]; then
	echo "Don't execute this file, source it with"
	echo "source $0"
	exit
fi

# This should be the user@hostname for your dev target
# In this case I have an ssh config for pmos
# Set this in your environment, or leave the default
[[ -z $SSH_TARGET ]] && SSH_TARGET="pmos"

exec 3>&1

LOGGING=true
LOGFILE="/tmp/automation-devboard.log"

HOST_IP=172.16.42.2
TARGET_IP=172.16.42.1

# Disables "warning: permanently added host"
alias ssht="ssh $SSH_TARGET -o LogLevel=ERROR -o \"ServerAliveInterval 1\" -o \"ServerAliveCountMax 2\""
#alias fslot='fastboot getvar current-slot 2>&1 | grep "current" | cut -d" " -f2'
#alias fserial='fastboot getvar serialno 2>&1 | grep "current" | cut -d" " -f2'

# $1: fastboot var to get
function at-fvar() {
	fastboot getvar $@ 2>&1 | grep "$@" | cut -d" " -f2
}

alias fslot="at-fvar current-slot"
alias fserial="at-fvar serialno"

function atlog() {
	if [ $LOGGING = true ]; then
		# only log to fd3 if it's writable
		echo ">> $@" | tee -a $LOGFILE 1>&3
	fi
}

# Get target state
# Possible return values:
# 'booted'
# 'bootloader'
# 'nc' (not connected)
function at-state() {
	if $(ip a | grep -q 172.16.42.2); then
		echo "booted"
		return
	elif [ $(fastboot devices | wc -l) -gt 0 ]; then
		echo "bootloader"
		return
	elif [ $(adb devices | wc -l) -gt 2 ]; then
		echo "adb"
		return
	fi
	echo 'nc'
}

# Wait for device to boot
function at-wait_booted() {
	while [ $(at-state) != "booted" ]; do
		sleep 0.5
	done
	atlog "Device booted, waiting for ssh"
	# Device is up, so safe to just attempt to connect
	# until sshd is up
	while ! $(echo -e "\n" | nc $TARGET_IP 22 2> /dev/null | grep -q "OpenSSH"); do
		sleep 0.2
	done
	atlog "SSH daemon runnning on device"
}

function at-wait_adb() {
	while [ $(at-state) != "adb" ]; do
		sleep 0.5
	done
	atlog "Device up on ADB"
}

# Run neofetch if available
# otherwise 'uname -a'
function at-version() {
	atlog "Gettting target version info"
	echo "
		[[ \$(which neofetch) ]] && neofetch || uname -a
	" | ssht
}

function at-nosudo() {
	atlog "Disabling sudo on target, you'll need your sudo password"
	ssht -t "sudo sed -i 's/%wheel ALL=(ALL) ALL/%wheel ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers && echo \"Done\""
}

function at-usbnet() {
	atlog "Enabling USB networking on target"
	echo "
		sudo ip route add default via 172.16.42.2
		echo nameserver 1.1.1.1 | sudo tee /etc/resolv.conf > /dev/null
	" | ssht
}

function at-trap-exit() {
	atlog "Exiting"
	return 5
}

# Run a command every time a device becomes available.
# Usually this should be a command like 'dmesg -w' or 'watch xyz'
function at-watch() {
	[[ -z $@ ]] && echo "You must provide a command to run!" && return 1
	atlog "Waiting for device to be booted"
	#trap at-trap-exit INT
	while : ; do
		at-wait_booted
		ssht -t $@
		atlog "Device disconnected, press Ctrl-C to exit"
		sleep 2
	done
}

function at-dmesg() {
	at-watch dmesg -w
}

# Install the reboot-mode package on pmos
# as it's needed for a lot of stuff here
function at-install_reboot_mode() {
	atlog "Installing reboot-mode package"
	echo "
		sudo apk add reboot-mode
	" | ssht
}

function at-setup() {
	atlog "Performing fresh-install setup on target"
	ssh-copy-id $SSH_TARGET
	at-nosudo
	at-install_reboot_mode
	atlog "Target is ready for use with 'at'"
	at-version
}

# Reboot device, with support for bootloader and recovery mode
# $1: [bootloader|recovery]
# $2: wait for for device to be detected
function at-reboot() {
	state=$(at-state)
	# Make sure target is connected and booted
	if [ "$state" = "booted" ]; then
		if [ -z $1 ]; then
			atlog "Rebooting"
			ssht sudo reboot
			return
		fi
		atlog "Rebooting to '$1'"
		if [ ! $(ssht which reboot-mode) ]; then
			echo "reboot-mode package missing, installing..."
			at-install_reboot_mode
		fi
		# Some trickery to stop ssh getting stuck here
		echo "
			sudo reboot-mode $1 &
			exit
		" | ssht
	elif [ "$state" = "adb" ]; then
		adb reboot $1
	fi
	if [ "$1" = "bootloader" ] && [ "$2" = "true" ]; then
		# fastboot will wait for the device to appear
		fslot > /dev/null
	fi
}

# Put the device in bootloader mode
# returns when the device is in bootlaoder
# mode
function at-to_bootloader() {
	state=$(at-state)
	slot=""
	if [ "$state" = "booted" ]; then
		at-reboot bootloader true
	elif [ "$state" = "adb" ]; then
		adb reboot bootloader
	fi
	slot=$(fslot)

	atlog "Current slot is $slot"
	if [ "$state" = "nc" ]; then
		return 1 # device not connected
	fi
}

# Flash a boot image
# $1: <path> the android boot image to flash
# $2: [true|false] reboot after flash
function at-flash() {
	[[ -z $1 ]] && echo "Please provide a boot image to flash" && return 1
	at-to_bootloader
	fastboot flash boot $1
	if [ "$2" != "false" ]; then
		fastboot reboot
	fi
}

## Recovery things

TWRP_ROOT=/home/caleb/android

# $1: if set, don't reboot to bootloader
function at-which() {
	state=$(at-state)
	if [ "$state" != "bootloader" ]; then
		if [ ! -z $1 ]; then
			return
		fi
		at-to_bootloader
	fi

	serialno=$(fserial)

	case $serialno in
	7c7e8b93)
		echo "fajita"
		return
		;;
	5725c671)
		echo "enchilada"
		return
		;;
	*)
		echo "unknown"
		return
		;;
	esac
}

# Reboot into TWRP, optionally pick which boot image and slot
# $1: SLOT
# $2: IMAGE
function at-twrp() {
	device=$(at-which)
	slot=$1
	image=$2
	if [ ! -z $slot ]; then
		fastboot set_active $slot
	fi
	if [ -z $image ]; then
		atlog "Booting: $(/usr/bin/ls $TWRP_ROOT/$device/twrp-3.3*)"
		fastboot boot $TWRP_ROOT/$device/twrp-3.3*
	else
		fastboot boot $image
	fi
}

# Reboot device into TWRP and flash a zip
# $1: zip to flash
# $2: if set, display recovery log while flashing
function at-flash_zip() {
	[[ -z $1 ]] && echo "Please specify a zip to flash" && return 1
	at-twrp a
	at-wait_adb
	adb push $1 /sdcard/
	zipname=$(basename $1)
	atlog "Flashing ZIP: $zipname"
	adb shell twrp install /sdcard/$zipname
	[[ ! -z $2 ]] && adb shell cat /tmp/recovery.log
	atlog "Flashed! Rebooting device"
	adb shell twrp reboot bootloader
	fastboot set_active b
}